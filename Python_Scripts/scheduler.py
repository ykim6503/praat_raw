# -*- coding: utf-8 -*-
import parselmouth

import pandas as pd
import os
import imp
import datetime
import numpy as np


from Python_Scripts.helper import constants as cst

imp.reload(cst)


DIR = cst.get_dirs()


# popuate data with a list of video IDs to be worked on
wav_list = []
for file in os.listdir(DIR['eqclinic_wav']):
    if file.endswith(".wav"):
        wav_list.append(file)

video_list = []
for s in wav_list:
    video_list.append(s[0:s.rfind('_')])

video_list = sorted(list(set(video_list)))


# iterate thru video_list and append results to csv
starting_index = 0
ending_index = 1540

for i in range(starting_index, ending_index+1):

    wav_name = wav_list[i]
    print('Analysing ' + wav_name)
    wav_path = os.path.join(DIR['eqclinic_wav'], wav_name)
    snd = parselmouth.Sound(wav_path)


    ###########################################################################
    # Intensity Section
    ###########################################################################
    intensity =  snd.to_intensity()
    intensity_df = pd.DataFrame(data =
                 {'time_sec':intensity.xs()
                 ,'intensity': np.squeeze(intensity.values.T)
                 }
                  )

    intensity =  snd.to_intensity()


    intensity_df = pd.DataFrame(data =
                 {'time_sec':intensity.xs()
                 ,'intensity': np.squeeze(intensity.values.T)
                 }
                  )

    # Reorder columns
    intensity_df = intensity_df[['time_sec', 'intensity']]

    intensity_trg = os.path.join(DIR['intensity_output'],
                                wav_name[:-4] #remove .wav extension
                                + '_praat_intensity.csv')

    intensity_df.to_csv(intensity_trg, index = False)

    ###########################################################################
    # Pitch Section
    ###########################################################################
    pitch = snd.to_pitch()
    frequency_df = pd.DataFrame(data =
                 {'time_sec': pitch.xs()
                 ,'frequency': pitch.selected_array['frequency']
                 })

    # Reorder columns
    frequency_df = frequency_df[['time_sec', 'frequency']]


    frequency_trg = os.path.join(DIR['pitch_output'],
                                wav_name[:-4] #remove .wav extension
                                + '_praat_pitch.csv')

    frequency_df.to_csv(frequency_trg, index = False)
