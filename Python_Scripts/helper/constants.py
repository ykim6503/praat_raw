'''
Returns constants to the project
'''

import os
import pandas as pd


def get_dirs():
    '''
    Returns constant directories paths
    '''
    dir_dict = dict()
    dir_dict['parent'] = os.path.abspath(os.path.join(os.path.dirname(__file__), '../..'))
    #dir_dict['eqclinic_wav'] = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../Data/'))
    dir_dict['eqclinic_wav'] = os.path.abspath(os.path.join(os.path.dirname(__file__), 'K:/eqclinic_770/'))
    dir_dict['intensity_output'] = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../Data/praat_output/intensity/'))
    dir_dict['pitch_output'] = os.path.abspath(os.path.join(os.path.dirname(__file__), '../../Data/praat_output/pitch/'))

    return dir_dict